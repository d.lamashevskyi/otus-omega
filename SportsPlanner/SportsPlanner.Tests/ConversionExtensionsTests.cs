﻿using AutoFixture;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using SportsPlanner.Server.Utils;
using SportsPlanner.Models.DTO;


namespace SportsPlanner.Tests
{
    public class ConversionExtensionsTests
    {
        class ChildEntity
        {
            public string? StrValue { get; set; }
            public Entity? Parent { get; set; } 


            public string? UnusedValue { get; set; }    
        }
        class Entity
        {
            public string? StrProp { get; set; }
            public Guid GuidProp { get; set; }

            public TimeOnly LessonTime { get; set; }
            public int UnusedProp { get; set; }
            public int IntProp { get; set; }

            public ICollection<ChildEntity> Children { get; set; } = new List<ChildEntity>();
        }


        class ChildShortModel
        {
            public string? StrValue { get; set; }

  
        }
        class ShortModel
        {
            public string? StrProp { get; set; }
            public Guid GuidProp { get; set; }

            public string? Unknown { get; set; }
            public int IntProp { get; set; }
            public TimeOnly LessonTime { get; set; }

        }

        class Model: ShortModel
        {
            public ICollection<ChildShortModel>? Children { get; set; } = new List<ChildShortModel>();

        }

        class ChildModel: ChildShortModel
        {
            public ShortModel Parent { get; set; }
        }

        class ParentDataModel
        {
            public string? StrProp { get; set; }
            public Guid GuidProp { get; set; }

            public string? Unknown { get; set; }
            public int IntProp { get; set; }

        }


        private void AssertEqual(Model source, Entity actual)
        {
            Assert.Equal(source.StrProp, actual.StrProp);
            Assert.Equal(source.IntProp, actual.IntProp);
            Assert.Equal(source.GuidProp, actual.GuidProp);
            Assert.Equal(source.LessonTime.Hour, actual.LessonTime.Hour);
            Assert.Equal(source.LessonTime.Minute, actual.LessonTime.Minute);
            if (source.Children == null)
            {
                Assert.Equal(0, actual.Children.Count);
            }
            else
            {
                Assert.Equal(source.Children.Count, actual.Children.Count);
                ChildEntity[] children = actual.Children.ToArray();
                ChildShortModel[] sourceChildren = source.Children.ToArray();
                for (int i = 0; i < source.Children.Count; i++)
                {
                    Assert.Equal(sourceChildren[i].StrValue, children[i].StrValue);
                }
            }
        }

        private void AssertEqual(Entity source, Model actual)
        {
            Assert.Equal(source.StrProp, actual.StrProp);
            Assert.Equal(source.IntProp, actual.IntProp);
            Assert.Equal(source.GuidProp, actual.GuidProp);
            Assert.Equal(source.LessonTime.Hour, actual.LessonTime.Hour);
            Assert.Equal(source.LessonTime.Minute, actual.LessonTime.Minute);
            //if (expected.Children == null)
            //{
            //    Assert.Equal(0, actual.Children.Count);
            //}
            //else
            //{
            Assert.Equal(source.Children.Count, actual.Children.Count);
                ChildShortModel[] children = actual.Children.ToArray();
                ChildEntity[] sourceChildren = source.Children.ToArray();
                for (int i = 0; i < source.Children.Count; i++)
                {
                    Assert.Equal(sourceChildren[i].StrValue, children[i].StrValue);
                }
            //}
        }

        private void AssertEqual(ChildModel source, ChildEntity actual)
        {
            Assert.Equal(source.StrValue, actual.StrValue);
            if(source.Parent == null)
            {
                Assert.True(actual.Parent == null);
            }
            else
            {
                Assert.True(actual.Parent != null);
                Assert.Equal(source.Parent.GuidProp, actual.Parent.GuidProp);
                Assert.Equal(source.Parent.IntProp, actual.Parent.IntProp);
                Assert.Equal(source.Parent.StrProp, actual.Parent.StrProp);
                Assert.Equal(source.Parent.LessonTime.Hour, actual.Parent.LessonTime.Hour);
                Assert.Equal(source.Parent.LessonTime.Minute, actual.Parent.LessonTime.Minute);
            }
        }

        private void AssertEqual(ChildEntity source, ChildModel actual) { 
            Assert.Equal(source.StrValue, actual.StrValue);
            if (source.Parent == null)
            {
                Assert.True(actual.Parent == null);
            }
            else
            {
                Assert.True(actual.Parent != null);
                Assert.Equal(source.Parent.GuidProp, actual.Parent.GuidProp);
                Assert.Equal(source.Parent.IntProp, actual.Parent.IntProp);
                Assert.Equal(source.Parent.StrProp, actual.Parent.StrProp);
                Assert.Equal(source.Parent.LessonTime.Hour, actual.Parent.LessonTime.Hour);
                Assert.Equal(source.Parent.LessonTime.Minute, actual.Parent.LessonTime.Minute);
            }
        }


        [Fact]
        public void ConvertsDTOWithChildrenToEntity()
        {
            var fixture = new Fixture();
            var dto = fixture.Build<Model>().Create();

            var entity = dto.ToEntity<Entity>();
            AssertEqual(dto, entity);

            dto.Children!.Clear();
            entity = dto.ToEntity<Entity>();
            AssertEqual(dto, entity);

            dto.Children = null;
            entity = dto.ToEntity<Entity>();
            AssertEqual(dto, entity);
        }

        [Fact]
        public void ConvertsEntityWithChildrenToDTO()
        {
            var fixture = new Fixture();
            Entity entity = fixture.Build<Entity>().Without(e => e.Children).Create();
            for(int i = 0; i < 3; ++i)
            {
                var child = fixture.Build<ChildEntity>().Without(ch =>ch.Parent).Create();
                entity.Children.Add(child);
                child.Parent = entity;
            }
            Model dto = entity.ToDTO<Model>();
            AssertEqual(entity, dto);

            entity.Children!.Clear();
            dto = entity.ToDTO<Model>();
            AssertEqual(entity, dto);

            //entity.Children = null;
            //dto = entity.ToDTO<Model>();
            //AssertEqual(entity, dto);
        }

        //[Fact]
        //public void ConvertsDTOWithParentToEntity()
        //{
        //    var fixture = new Fixture();
        //    ChildModel model = fixture.Build<ChildModel>().Create();
        //    ChildEntity entity = model.ToEntity<ChildEntity>();
        //    AssertEqual(model, entity);

        //    model.Parent = null;
        //    entity = model.ToEntity<ChildEntity>();
        //    AssertEqual(model, entity);
        //}

        [Fact]
        public void ConvertsEntityWithParentToDTO()
        {
            var fixture = new Fixture();
            ChildEntity entity = fixture.Build<ChildEntity>().Without(e => e.Parent).Create();
            Entity parentEntity = fixture.Build<Entity>().Without(e => e.Children).Create();
            parentEntity.Children.Add(entity);
            entity.Parent = parentEntity;

            ChildModel model = entity.ToDTO<ChildModel>();
            AssertEqual(entity, model);

            entity.Parent = null;
            model = entity.ToDTO<ChildModel>();
            AssertEqual(entity, model);
        }

    }
}
