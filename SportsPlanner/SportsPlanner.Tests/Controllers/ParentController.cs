﻿using System;
using System.Collections.Generic;
using AutoFixture;
using AutoFixture.AutoMoq;
using FluentAssertions;
using Xunit;
using Moq;
using SportsPlanner.Models.DTO;
using Microsoft.AspNetCore.Mvc;
using SportsPlanner.DataAccess.Model;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.DataAccess;
using SportsPlanner.Core.Abstractions.Repositories;
using SportsPlanner.Server.Utils;
using SportsPlanner.Server.Controllers;
using System.Diagnostics.Metrics;
using Azure.Core;

namespace SportsPlanner.Tests.Controllers
{
    public class ParentController
    {
        private readonly Mock<IRepository<Parent>> _parentMock;
        private readonly SportsPlanner.Server.Controllers.ParentController _parentController;
        public ParentController()
        {
            var fixture = new Fixture().Customize(new AutoMoqCustomization());
            _parentMock = fixture.Freeze<Mock<IRepository<Parent>>>();

            _parentController = fixture.Build<SportsPlanner.Server.Controllers.ParentController>().OmitAutoProperties().Create();
        }
        public Parent CreateBaseParent()
        {
            var partner = new Parent()
            {
                Id = "7d994823-8226-4273-b063-1a95f3cc1df8",
                LastName = "Фамилия",
                FirstName = "Имя",
                SurName = "Отчество",
                Phone = "+79222455462",
                Telegram = "@yttwe$t",
                Email = "test@ya.ru",
                IsDeleted = false
            };
            return partner;
        }
        [Fact]
        public async void ParentController_GetParentById_IdIsNull_ReturnsBadRequest()
        {
            // Arrange
            var partnerId = "";
            Parent parent = CreateBaseParent();

            _parentMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(parent);

            // Act
            var result = await _parentController.GetParentById(partnerId);

            // Assert
             result.Should().NotBeNull();
            var actionResult = result.Result as CreatedAtActionResult;
             Assert.Equal(400, ((ObjectResult)((ActionResult<ParentModel>)result).Result).StatusCode);
        }
        [Fact]
        public async void ParentController_GetParentById_ParentIsNotFound_ReturnsNotFound()
        {
            // Arrange
            var partnerId = "def47943-7aaf-44a1-ae21-05aa4948b165";
            Parent parent = CreateBaseParent();

            _parentMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(parent);

            // Act
            var result = await _parentController.GetParentById(partnerId);

            // Assert
            result.Value.Should().BeNull();
        }
        [Fact]
        public async void ParentController_GetParentById_ParentHasValue_ReturnsNotBeNull()
        {
            // Arrange
            var partnerId = "7d994823-8226-4273-b063-1a95f3cc1df8";
            Parent parent = CreateBaseParent();

            _parentMock.Setup(repo => repo.GetByIdAsync(partnerId)).ReturnsAsync(parent);

            // Act
            var result = await _parentController.GetParentById(partnerId);

            // Assert
            result.Should().NotBeNull();
        }
        [Fact]
        public async void ParentController_GetParentList_returnNotNull()
        {
            // Arrange
            Parent parent = CreateBaseParent();
            // Act
            var result = await _parentController.GetParentList();
            // Assert
           Assert.NotNull(result);
        }
        //сохранили нового родителя в базу данных
        [Fact]
        public async void ParentController_AddParent_ExpectedId()
        {
            // Arrange
            var newparentId = "7d993373-8337-4273-b063-1a95111c1ae8";
            ParentModel newParent = new ParentModel() { Id = newparentId, LastName ="Токарев", FirstName = "Евгений", SurName="Олегович", Email="email@mail.ru", Phone="+79125728899", Telegram="" };
            
            // Act
            var result = await _parentController.AddParent(newParent);

            //Assert
            result.Should().Be(newparentId);
        }
    }
}
