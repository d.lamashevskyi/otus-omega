﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using IdentityServer.Models.Identity;

namespace IdentityServer.DataAccess.Models
{

    /// <summary>
    /// Справочник тренеров
    /// </summary>
    public class Teacher : BaseUser
    {


        [Display(Name = "Дата начала работы")]
        public DateTime? BeginDate { get; set; }

        [Display(Name = "Дата конца работы")]
        public DateTime? EndDate { get; set; }

        // login к личному кабинету, пароль к личному кабинету ,
    }
}
