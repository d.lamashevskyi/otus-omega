﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Core.Domain
{
    public class BaseEntity
    {
        public string Id { get; set; }
    }
}
