﻿using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.Core.Domain;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.StaticData;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Reflection.Emit;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.DataAccess
{
    public class SportsPlannerDbContext : IdentityDbContext
    {
        public DbSet<ChildVisit> ChildVisits { get; set; }
        public DbSet<ScheduleChange> ScheduleChanges { get; set; }
        public DbSet<GroupStudent> GroupStudents { get; set; }
        public DbSet<Payment> Payments { get; set; }
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Tariff> Tariffs { get; set; }
        public DbSet<ScheduleEntry> ScheduleEntries { get; set; }
        public DbSet<Section> Sections { get; set; }

        public DbSet<Group> Groups { get; set; }
        public DbSet<Student> Students { get; set; }
        public DbSet<Teacher> Teachers { get; set; }

        public SportsPlannerDbContext(DbContextOptions<SportsPlannerDbContext> options)
            : base(options)
        {
        }


        protected override void OnModelCreating(ModelBuilder builder)
        {
            InitDataBase(builder);
            //builder.Entity<GroupStudent>().HasKey(x => new { x.StudentId, x.GroupId });
            //builder.Entity<GroupStudent>().HasOne(x => x.Student).WithMany(p => p.Groups).HasForeignKey(pt => pt.StudentId);
            //builder.Entity<GroupStudent>().HasOne(pt => pt.Group).WithMany(t => t.Students).HasForeignKey(pt => pt.GroupId);
            base.OnModelCreating(builder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }
        private void InitDataBase(ModelBuilder builder)
        {
            var adminRoleid = Guid.NewGuid().ToString();
            var adminUserid = Guid.NewGuid().ToString();
            builder.Entity<IdentityRole>().HasData(new IdentityRole[]
            {
              new  IdentityRole{ Id = adminRoleid, Name = StaticData.AdminRole, NormalizedName = StaticData.AdminRole.ToUpper()},
              new  IdentityRole{Id = Guid.NewGuid().ToString(),  Name = StaticData.TeacherRole, NormalizedName = StaticData.TeacherRole.ToUpper()},
              new  IdentityRole{Id = Guid.NewGuid().ToString(),  Name = StaticData.ParentRole, NormalizedName = StaticData.ParentRole.ToUpper()}
            });

        }
    }
}
