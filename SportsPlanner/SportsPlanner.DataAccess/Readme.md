﻿## Миграции

Команда для создания БД из миграций: dotnet ef database update --startup-project <путь к проекту>
e.g. dotnet ef database update --startup-project <путь к проекту>

Основной проект для миграций SportsPlanner.DataAccess.Model

### Полезные команды (для копипаста)
#### Подключить миграции
   dotnet ef migrations add InitialMigration --startup-project <путь к проекту>

#### Добавить миграцию
   dotnet ef migrations add <name> --startup-project <путь к проекту>

#### Обновить базу
   dotnet ef database update --startup-project <путь к проекту>

#### Обновить базу на конкретную миграцию
   dotnet ef database update <name> --startup-project <путь к проекту>

#### Удалить последнюю миграцию
   dotnet ef migrations remove --startup-project <путь к проекту>

#### Update EF Core Tools
   dotnet tool update --global dotnet-ef

