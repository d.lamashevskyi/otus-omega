﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.Core.Abstractions.Repositories;
using SportsPlanner.Core.Domain;
using SportsPlanner.Models.Identity;
using System;
using System.Collections;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.DataAccess.Repositories
{
    public class EfRepository<T> : IRepository<T> where T : BaseUser
    {
        private readonly SportsPlannerDbContext _dataContext;

        public EfRepository(SportsPlannerDbContext dataContext)
        {
            _dataContext = dataContext;
        }
        public async Task<IEnumerable<T>> GetAllAsync()
        {  
            var entities = await _dataContext.Set<T>().Where(t => !t.IsDeleted).ToListAsync();
            return entities;
        }
        public async Task<T> GetByIdAsync(string id) 
        {
            var entity = await _dataContext.Set<T>().FirstOrDefaultAsync(x => x.Id == id & !x.IsDeleted);
            return entity;
        }
        public async Task<IEnumerable<T>> GetRangeByIdsAsync(List<string> ids) 
        {
            var entities = await _dataContext.Set<T>().Where(x => ids.Contains(x.Id) & !x.IsDeleted).ToListAsync();
            return entities;
        }
        //Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);
        //Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
        public async Task AddAsync(T entity)
        {
            await _dataContext.Set<T>().AddAsync(entity);
            await _dataContext.SaveChangesAsync();
        }
        public async Task DeleteAsync(T entity)
        {
            _dataContext.Set<T>().Remove(entity);
            await _dataContext.SaveChangesAsync();
        }
        public async Task isDeleteAsync(T entity)
        {
            if (entity != null) entity.IsDeleted =true;
            await _dataContext.SaveChangesAsync();
        }
        public async Task UpdateAsync(T entity)
        {
            await _dataContext.SaveChangesAsync();
        }
    }
}
