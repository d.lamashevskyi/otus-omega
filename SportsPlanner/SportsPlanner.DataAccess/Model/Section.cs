﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    /// <summary>
    /// Справочник названий секций
    /// </summary>
    public class Section: BaseEditedEntity
    {
        [MaxLength(100)]
        [Display(Name = "Наименование")]
        public string Name { get; set; }

        [MaxLength(200)]
        [Display(Name = "Описание")]
        public string Description { get; set; }

        public ICollection<Group> Groups { get; set; } = new List<Group>();

    }
}
