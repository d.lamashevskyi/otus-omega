﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class Payment : BaseEditedEntity
    {
        [Display(Name = "Родитель")]
        public string ParentId { get; set; }
        [ForeignKey("ParentId")]
        public Parent Parent { get; set; }
        [Display(Name = "Ученик")]
        public string GroupStudentId { get; set; }
        [ForeignKey("GroupStudentId")]
        public GroupStudent GroupStudent { get; set; }
        [Display(Name = "Секция")]
        public DateTime PaymentDate { get; set; }
        
        [Display(Name = "Сумма платежа")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Amount { get; set; }

        public Tariff Tariff { get; set; }
    }
}
