﻿using Microsoft.AspNetCore.Identity;
using SportsPlanner.Core.Domain;
using SportsPlanner.Models.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class Parent : BaseUser
    {
        public string Telegram;

        public string? Phone { get; set; }

        // login к личному кабинету, пароль к личному кабинету ?

        public ICollection<Student> Children { get; set; } = new List<Student>();
    }
}
