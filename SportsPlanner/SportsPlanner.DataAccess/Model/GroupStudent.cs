﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class GroupStudent : BaseEditedEntity
    {
        [Display(Name = "Ученик")]
        public string StudentId { get; set; }
        public Student Student { get; set; }
        [Display(Name = "Группа")]
        public string GroupId { get; set; }
        public Group Group { get; set; }
        
        [Display(Name = "Остаток занятий")]
        public int PaidLessonsRest { get; set; }
        
        //[Display(Name = "Дата начала действия")]
        //public DateOnly BeginDate { get; set; }
        //[Display(Name = "Дата конца действия")]
        //public DateOnly? EndDate { get; set; }
    }
}
