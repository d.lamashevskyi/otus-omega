﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    /// <summary>
    /// справочник учеников
    /// </summary>
    public class Student : BaseEditedEntity
    {
        [MaxLength(100)]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [MaxLength(100)]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [MaxLength(100)]
        [Display(Name = "Отчество")]
        public string? SurName { get; set; }
        [Display(Name = "ФИО")]
        public string Name => $"{LastName} {FirstName} {SurName}";
        [Display(Name = "Дата рождения")]
        public DateOnly? Birthdate { get; set; }

        [Display(Name = "Родитель")]
        public string ParentId { get; set; }
        [ForeignKey("ParentId")]
        public Parent Parent { get; set; }

        //[Display(Name = "Дата начала занятий")]
        //public DateOnly BeginDate { get; set; }
        //[Display(Name = "Дата прекращения занятий")]
        //public DateOnly? EndDate { get; set; }
        public ICollection<GroupStudent> Groups { get; set; } = new List<GroupStudent>();
    }
}
