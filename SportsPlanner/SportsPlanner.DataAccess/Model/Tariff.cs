﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class Tariff : BaseEditedEntity
    {
        [Display(Name = "Секция")]
        public string SectionId { get; set; }
        [ForeignKey("SectionId")]
        public Section Section { get; set; }

        [Display(Name = "Стоимость тарифного плана")]
        [Column(TypeName = "decimal(18, 2)")]
        public decimal Cost { get; set; }

        [Display(Name = "Количество входящих в тариф (абонемент) занятий")]
        public int LessonQuantity { get; set; }

        [Display(Name = "Дата начала действия")]
        public DateOnly BeginDate { get; set; }

        [Display(Name = "Дата конца действия")]
        public DateOnly? EndDate { get; set; }

    }
}
