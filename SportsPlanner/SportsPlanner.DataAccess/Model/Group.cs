﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class Group: BaseEditedEntity
    {
        [ForeignKey("SectionId")]
        public Section? Section { get; set; }

        public string SectionId { get; set; }

        public string? Name { get; set; }

        public ICollection<GroupStudent> Students { get; set; } = new List<GroupStudent>();

        [Display(Name = "Тренер")]
        public string TeacherId { get; set; }

        [ForeignKey("TeacherId")]
        public Teacher Teacher { get; set; }

        public ICollection<ScheduleEntry> BaseSchedule { get; set; } = new List<ScheduleEntry>();
    }
}
