﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using SportsPlanner.Core.Domain;
using Microsoft.AspNetCore.Identity;
using SportsPlanner.Models.Identity;

namespace SportsPlanner.DataAccess.Model
{

    /// <summary>
    /// Справочник тренеров
    /// </summary>
    public class Teacher : BaseUser
    {


        [Display(Name = "Дата начала работы")]
        public DateOnly? BeginDate { get; set; }

        [Display(Name = "Дата конца работы")]
        public DateOnly? EndDate { get; set; }

        // login к личному кабинету, пароль к личному кабинету ,
    }
}
