﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    /// <summary>
    /// Рассписание групп
    /// Таблица рассписания групп: ID названия секции, id тренера, дни занятий (1-понедельние, 2 вторник... через запятую списком), время, период действия с по
    /// </summary>
    public class ScheduleEntry : BaseEditedEntity
    {
        [Display(Name = "Группа")]
        public string GroupId { get; set; }
        
        [ForeignKey("GroupId")]
        public Group Group { get; set; }


        [Display(Name = "День недели занятий")]
        public int DayOfWeek { get; set; }
        [Display(Name = "Время начала занятий")]
        public TimeOnly LessonTime { get; set; }
        
        [Display(Name = "Длительность занятия, мин")] 
        public int Duration { get; set; }
        [Display(Name = "Дата начала действия")]
        public DateOnly BeginDate { get; set; }

        [Display(Name = "Дата конца действия")]
        public DateOnly? EndDate { get; set; }
    }
}
