﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;
using System.ComponentModel;

namespace SportsPlanner.DataAccess.Model
{
    public enum ScheduleChangeType
    {
        [Display(Name = "Каникулы")]
        Holidays = 1, 
        [Display(Name = "Праздник")]
        PublicHoliday = 2,
        [Display(Name = "Отсутствие преподавателя")]
        NoTeacher = 3,
        [Display(Name = "Перенос рабочего дня")]
        WorkDayMove = 4,
        [Display(Name = "Отмена по другой причине")]
        CancelledForSomeReason = 5,
        [Display(Name = "Дополнительное занятие")]
        ExtraLesson = 6
    }
    public class ScheduleChange : BaseEditedEntity
    {
        [Display(Name = "Секция")]
        public string GroupId { get; set; }
        [ForeignKey("GroupId")]
        public Group Group { get; set; }
        [Display(Name = "Дата отмены/добавления занятия")] 
        public DateTime ChangeDateTime { get; set; }
        
        public int? ExtraLessonDuration { get; set; }

        [Display(Name = "Причина изменения расписания")]
        public ScheduleChangeType ChangeType { get; set; }
    }
}
