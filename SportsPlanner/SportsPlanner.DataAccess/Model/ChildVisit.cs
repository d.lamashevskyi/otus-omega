﻿using SportsPlanner.Core.Domain;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.DataAccess.Model
{
    public class ChildVisit : BaseEditedEntity
    {
        [Display(Name = "Секция")]
        public string SectionId { get; set; }
        [ForeignKey("SectionId")]
        public Section Section { get; set; }

        [Display(Name = "Ученик")]
        public string? StudentId { get; set; }
        [ForeignKey("StudentId")]
        public Student Student { get; set; }
        [Display(Name = "Дата визита")]
        public DateTime DateVisit {get; set; }
    }
}
