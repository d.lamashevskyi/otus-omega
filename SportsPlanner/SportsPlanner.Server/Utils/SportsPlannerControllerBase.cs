﻿using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore.ChangeTracking;
using Microsoft.Identity.Web;
using SportsPlanner.Core.Domain;
using SportsPlanner.DataAccess;

namespace SportsPlanner.Server.Utils
{
    public class SportsPlannerControllerBase: ControllerBase
    {
        protected readonly SportsPlannerDbContext _db;

        public SportsPlannerControllerBase(SportsPlannerDbContext db)
        {
            _db = db;
            _db.SavingChanges += BeforeSavingData;
        }

        private void BeforeSavingData(object? sender, SavingChangesEventArgs e)
        {
            DateTime now = DateTime.Now;
            string? user = null;
            foreach (var trackedEntry in _db.ChangeTracker.Entries().Where(entry => entry.State == EntityState.Modified || entry.State == EntityState.Added))
            {
                if(user == null)
                {
                    user = HttpContext.User?.FindFirst("nameid")?.Value;
                    if(user == null) user = "Anoymous";                    

                }
                if (trackedEntry.Entity is BaseEditedEntity editableEntity)
                {
                    editableEntity.Modified = now;
                    editableEntity.ModifiedBy = user;

                    if (trackedEntry.State == EntityState.Added)
                    {
                        editableEntity.Created = now;
                        editableEntity.CreatedBy = user;
                    }
                }
            }
        }
    }
}
