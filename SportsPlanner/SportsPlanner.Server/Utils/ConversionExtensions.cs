﻿using System.Reflection;
using System.Collections;
using System.Diagnostics;

namespace SportsPlanner.Server.Utils
{
    public static class ConversionExtensions 
    {
        public static TDTO ToDTO<TDTO>(this object entity) where TDTO: class, new() 
        {
            TDTO result = new TDTO();
            CopyProperties(result, entity);
            return result;
        }

        public static TEntity ToEntity<TEntity>(this object dto) where TEntity : class, new()
        {
            TEntity result = new TEntity();
            CopyProperties(result, dto);
            return result;
        }

        public static void AssignDTO(this object entity, object dto)
        {
            CopyProperties(entity, dto);
        }

        private static bool IsCollectionType(Type type, out Type? itemType)
        {
            itemType = null;
            if (!type.IsGenericType) return false;
            if (type.IsGenericTypeDefinition) return false;
            var supportedInterfaces = type.GetInterfaces();
            foreach ( var supportedInterface in supportedInterfaces)
            {
                if (supportedInterface.IsGenericType)
                {
                    if(typeof(IEnumerable<>) == supportedInterface.GetGenericTypeDefinition())
                    {
                        itemType = supportedInterface.GenericTypeArguments[0];
                        return true;
                    }
                }
            }
            return false;
        }

        private static IList InstantiateList(Type itemType)
        {
            return (IList)typeof(List<>).MakeGenericType(itemType).GetConstructor(new Type[0])!.Invoke(new object[0]);
        }

        private static void CopyProperties(object dest, object src)
        {
            foreach (var prop in dest.GetType().GetProperties(BindingFlags.Public | BindingFlags.SetProperty | BindingFlags.Instance))
            {
                PropertyInfo? srcProp = src.GetType().GetProperty(prop.Name, BindingFlags.Public | BindingFlags.Instance | BindingFlags.GetProperty);
                if (srcProp != null)
                {
                    if (IsCollectionType(prop.PropertyType, out var itemType) && IsCollectionType(srcProp.PropertyType, out var srcItemType))
                    {
                        IEnumerable? srcCollection = srcProp.GetValue(src) as IEnumerable;
                        if (srcCollection == null && srcProp.GetValue(src) != null) throw new InvalidOperationException("Can assign only from collections implementing IEnumerable");
                        IList? destList = prop.GetValue(dest) as IList;
                        if (destList == null && prop.GetValue(dest) != null) throw new InvalidOperationException("Can assign only to collections implementing IList.");
                        if (destList == null)
                        {
                            destList = InstantiateList(itemType!);
                            prop.SetValue(dest, destList);
                        }
                        else
                        {
                            destList.Clear();
                        }
                        if (srcCollection != null)
                        {
                            foreach (object srcItem in srcCollection)
                            {
                                object destItem = itemType!.GetConstructor(new Type[0])!.Invoke(new object[0]);
                                CopyProperties(destItem, srcItem);
                                destList.Add(destItem);
                            }
                        }

                    }
                    else if (prop.PropertyType.Equals(srcProp.PropertyType))
                    {
                        prop.SetValue(dest, srcProp.GetValue(src));
                    }
                    else
                    { 
                        object? srcChild = srcProp.GetValue(src);
                        if (srcChild == null)
                        {
                            prop.SetValue(dest, null);
                        }
                        else
                        {
                            object? destChild = Activator.CreateInstance(prop.PropertyType);
                            //Debug.Assert(destChild != null);
                            //object destChild = prop.PropertyType.GetConstructor(BindingFlags.Public | BindingFlags.NonPublic | BindingFlags., new Type[0])!.Invoke(new object[0]);
                            CopyProperties(destChild!, srcChild);
                            prop.SetValue(dest, destChild);
                        }
                        
                    }
                }
            }

        }

 
    }
}
