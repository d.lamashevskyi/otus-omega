using AutoMapper;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Identity;
using Microsoft.EntityFrameworkCore;
using Microsoft.IdentityModel.Tokens;
using Microsoft.OpenApi.Models;
using SportsPlanner.Core.Abstractions;
using SportsPlanner.DataAccess;
using SportsPlanner.Models.Authentication;
using SportsPlanner.Server.Middleware;
using SportsPlanner.Services.JWT;
using SportsPlanner.Services.Mapping;
using System.Data;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authorization;
using SportsPlanner.Models.Authorization;
using SportsPlanner.Core.Abstractions.Repositories;
using SportsPlanner.DataAccess.Repositories;

var builder = WebApplication.CreateBuilder(args);

builder.Services.Configure<JwtSchemeOptions>(builder.Configuration.GetSection("SecretSettings"));


builder.Services
    .AddDbContext<SportsPlannerDbContext>(options => options.UseSqlite(builder.Configuration.GetConnectionString("SportsPlannerDatabase")));

builder.Services.AddIdentityCore<IdentityUser>(options =>
                                       options.SignIn.RequireConfirmedAccount = true)
                .AddEntityFrameworkStores<SportsPlannerDbContext>();

InstallAutomapper(builder.Services);

builder.Services.AddScoped<IJWTService, JWTService>();

builder.Services.AddAuthentication(JwtBearerDefaults.AuthenticationScheme)
    .AddCustomJwtToken();



builder.Services.AddAuthorization(options =>
{
    options.AddPolicy("Admin", policy => policy.Requirements.Add(new AdminRequirement()));
});
builder.Services.AddScoped<IAuthorizationHandler, AdminRequirementHandler>();

builder.Services.AddScoped(typeof(IRepository<>), typeof(EfRepository<>));//854

builder.Services.AddControllers(options => options.SuppressAsyncSuffixInActionNames = false);
// Learn more about configuring Swagger/OpenAPI at https://aka.ms/aspnetcore/swashbuckle
builder.Services.AddEndpointsApiExplorer();

builder.Services.AddSwaggerGen(options =>
{
    options.AddSecurityDefinition("Bearer", new OpenApiSecurityScheme
    {
        Description = "JWT Authorization header using the Bearer scheme. Example: \"Authorization: Bearer {token}\"",
        Name = "Authorization",
        In = ParameterLocation.Header,
        Type = SecuritySchemeType.ApiKey,
        Scheme = "Bearer",
        BearerFormat = "JWT"
    });
    options.AddSecurityRequirement(new OpenApiSecurityRequirement
              {
                {
                    new OpenApiSecurityScheme
                    {
                        Reference = new OpenApiReference
                        {
                            Type = ReferenceType.SecurityScheme,
                            Id = JwtBearerDefaults.AuthenticationScheme
                        }
                    },
                    Array.Empty<string>()
                }
            });
});

var app = builder.Build();

// Configure the HTTP request pipeline.
if (app.Environment.IsDevelopment())
{
    app.UseSwagger();
    app.UseSwaggerUI();
}

app.UseHttpsRedirection();

app.UseAuthentication();
//app.UseIdentityServer();
app.UseAuthorization();


app.MapControllers();

app.Run();

static IServiceCollection InstallAutomapper(IServiceCollection services)
{
    services.AddSingleton<IMapper>(new Mapper(GetMapperConfiguration()));
    return services;
}

static MapperConfiguration GetMapperConfiguration()
{
    var configuration = new MapperConfiguration(cfg =>
    {
        cfg.AddProfile<RegistrationMappingProfile>();

    });
    configuration.AssertConfigurationIsValid();
    return configuration;
}