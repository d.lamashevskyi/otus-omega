﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using SportsPlanner.DataAccess;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.StaticData;
using SportsPlanner.Models.DTO;
using SportsPlanner.Server.Utils;
using Microsoft.EntityFrameworkCore;

namespace SportsPlanner.Server.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme, Policy = "Admin")]
    public class SectionController : SportsPlannerControllerBase
    {
        public SectionController(SportsPlannerDbContext db) : base(db)
        {

        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<SectionShortModel>>> GetSectionListAsync()
        {
            var list = await _db.Sections.Where(s => !s.IsDeleted).ToListAsync();
            return Ok(list.Select(s => s.ToDTO<SectionShortModel>()));
        }


        [HttpGet("{id:guid}")]
        public async Task<ActionResult<SectionModel>> GetSectionByIdAsync(string id)
        {
            var section = await _db.Sections.Where(s => id.Equals(s.Id)).FirstOrDefaultAsync();
            if (section == null) return NotFound();
            return Ok(section.ToDTO<SectionModel>());
        }

        [HttpPost]
        public async Task<IActionResult> AddSectionAsync(SectionUpdateRequestModel sectionModel)
        {
            var section = sectionModel.ToEntity<Section>();
            await _db.Sections.AddAsync(section);
            var res = await _db.SaveChangesAsync() > 0;

            if (!res)
                return BadRequest("Не удалось сохранить группу!");

            return Ok(section.Id);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult> ModifySectionAsync(string id, SectionUpdateRequestModel sectionData)
        {
            var section = await _db.Sections.Where(s => String.Equals(id, s.Id) && !s.IsDeleted).FirstOrDefaultAsync();
            if (section == null) return NotFound();
            section.AssignDTO(sectionData);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpGet("{id:guid}/groups")]
        public async Task<ActionResult<IEnumerable<GroupShortModel>>> GetSectionGroupsAsync (string id){
            var groups = await _db.Groups.Where(g => id.Equals(g.SectionId) && !g.IsDeleted).Include(g => g.Section).Include(g => g.Teacher).ToListAsync();
            return Ok(groups.Select(g => g.ToDTO<GroupShortModel>()));
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteSectionAsync(string id)
        {
            var section = await _db.Sections.Where(s => id.Equals(s.Id) && !s.IsDeleted).Include(s => s.Groups).FirstOrDefaultAsync();
            if (section == null) return NotFound();
            if(section.Groups.Count > 0)
            {
                return BadRequest(new ErrorModel("Cannot remove the section while there are still groups in this section."));
            }
            section.IsDeleted = true;
            await _db.SaveChangesAsync();
            return NoContent();

        }
    }
}
