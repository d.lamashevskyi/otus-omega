﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.Core.Abstractions.Repositories;
using SportsPlanner.DataAccess;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.DTO;
using SportsPlanner.Server.Utils;

namespace SportsPlanner.Server.Controllers
{
    [ApiController]
    [Route("api/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme)]
    public class ParentController : SportsPlannerControllerBase
    {
        private readonly IRepository<Parent> _parent;
        public ParentController(SportsPlannerDbContext db, IRepository<Parent> parent) : base(db)
        {
            _parent = parent;
        }
        /// <summary>
        /// Получить родителя по id
        /// </summary>
        [HttpGet("{id:guid}")]
        public async Task<ActionResult<ParentModel>> GetParentById(string id)
        {
            if (string.IsNullOrEmpty(id)) return BadRequest("Введите Id родителя");
            if (_parent == null) return NotFound();
            Parent parent = await _parent.GetByIdAsync(id); 
            if (parent == null) return NotFound();
            ParentModel result = parent.ToDTO<ParentModel>();
            return Ok(result);
        }
        /// <summary>
        /// Получить список родителей
        /// </summary>
        /// <returns></returns>
        [HttpGet]
        public async Task<IEnumerable<ParentModel>> GetParentList()
        {
            var parents = await _parent.GetAllAsync();            
            return parents.Select(t => t.ToDTO<ParentModel>());
        }
        /// <summary>
        /// Добавить родителя
        /// </summary>
        /// <returns></returns>
        [HttpPost]
        public async Task<string> AddParent(ParentModel parentData)
        {
            if (string.IsNullOrEmpty(parentData.Id)) parentData.Id = Guid.NewGuid().ToString();
            var parent = parentData.ToEntity<Parent>();
            await _parent.AddAsync(parent);
            return parent.Id;
        }
        /// <summary>
        /// Изменить данные родителя
        /// </summary>
        /// <returns></returns>
        [HttpPut]
        public async Task<ActionResult> ModifyParent(ParentModel parentData)
        {
            var parent = await _parent.GetByIdAsync(parentData.Id);
            if (parent == null) return NotFound();
            parent.AssignDTO(parentData);
            await _db.SaveChangesAsync();
            return NoContent();
        }
        /// <summary>
        /// Удалить родителя
        /// </summary>
        /// <returns></returns>
        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteParent(string id)
        {
            var parent = await _parent.GetByIdAsync(id); 
            if (parent == null) return NotFound();
            await _parent.isDeleteAsync(parent);
            return NoContent();
        }
    }
}
