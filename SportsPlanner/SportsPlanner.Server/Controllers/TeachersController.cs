﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.DataAccess;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.DTO;
using SportsPlanner.Server.Utils;

namespace SportsPlanner.Server.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    [Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme/*,Roles ="Admin"*/)]
    public class TeachersController : SportsPlannerControllerBase
    {

        public TeachersController(SportsPlannerDbContext db) : base(db)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<TeacherModel>> GetTeacherById(string id)
        {
            Teacher? teacher = await _db.Teachers.FindAsync(id);
            if (teacher == null) return NotFound();
            TeacherModel result = teacher.ToDTO<TeacherModel>();
            return Ok(result);
        }

        [HttpGet]
        public async Task<IEnumerable<TeacherShortModel>> GetTeacherList()
        {
            var teachers = await _db.Teachers.Where(teacher => !teacher.IsDeleted).ToListAsync();
            return teachers.Select(teacher => teacher.ToDTO<TeacherShortModel>());
        }

        [HttpPost]
        public async Task<string> AddTeacher(CreateTeacherRequestModel teacherData)
        {
            var teacher = teacherData.ToEntity<Teacher>();
            await _db.Teachers.AddAsync(teacher);
            await _db.SaveChangesAsync();
            return teacher.Id;
        }

        [HttpPut]
        public async Task<ActionResult> ModifyTeacher(TeacherModel teacherData)
        {
            var teacher = await _db.Teachers.FirstOrDefaultAsync(t => t.Id == teacherData.Id && !t.IsDeleted);
            if (teacher == null) return NotFound();
            teacher.AssignDTO(teacherData);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteTeacher(string id)
        {
            var teacher = await _db.Teachers.FindAsync(id);
            if (teacher == null) return NotFound();
            teacher.IsDeleted = true;
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpGet("{id:guid}/schedule")]
        public Task<ActionResult<ICollection<TeacherScheduleEntryModel>>> GetTeacherSchedule([FromQuery] DateTime startDate, [FromQuery] DateTime endDate)
        {
            throw new NotImplementedException();
        }
    }
}
