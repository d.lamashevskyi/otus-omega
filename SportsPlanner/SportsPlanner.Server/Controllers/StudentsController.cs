﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using SportsPlanner.DataAccess;
using SportsPlanner.Server.Utils;
using SportsPlanner.Models.DTO;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.DataAccess.Model;

namespace SportsPlanner.Server.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme/*,Roles ="Admin"*/)]
    public class StudentsController: SportsPlannerControllerBase
    {
        public StudentsController(SportsPlannerDbContext db) : base(db)
        {
        }

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<StudentModel>> GetStudentById(string id) {
            var student = await _db.Students.Where(s => s.Id == id).Include(s => s.Groups).FirstOrDefaultAsync();
            if(student == null) return NotFound();
            return Ok(student.ToDTO<StudentModel>());
        }

        [HttpGet]
        public async Task<ActionResult<IEnumerable<StudentShortModel>>> GetStudentList()
        {
            var students = await _db.Students.Where(s => !s.IsDeleted).ToListAsync();

            return Ok(students.Select(student => student.ToEntity<StudentShortModel>()));
        }

        [HttpPost]
        public async Task<ActionResult<string>> AddStudent(StudentUpdateRequestModel studentData)
        {
            if (!String.IsNullOrEmpty(studentData.ParentId))
            {
                if(await _db.Parents.Where(parent => parent.Id.Equals(studentData.ParentId) && !parent.IsDeleted).FirstOrDefaultAsync() == null)
                {
                    return BadRequest(new ErrorModel() { ErrorMessage = $"No parent with such an id: {studentData.ParentId}"});
                }
            }
            var student = studentData.ToEntity<Student>();
            student.Id = Guid.NewGuid().ToString();
            await _db.Students.AddAsync(student);
            await _db.SaveChangesAsync();
            return Ok(student.Id);
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult> ModifyStudent(string id, StudentUpdateRequestModel studentData)
        {
            var student = await _db.Students.Where(s => String.Equals(id, s.Id)).Include(s => s.Groups).FirstOrDefaultAsync();
            if (student == null) return NotFound();
            student.AssignDTO(studentData);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{id:guid}")]
        public async Task<IActionResult> DeleteStudent(string id)
        {
            var student = await _db.Students.FindAsync(id);
            if (student == null) return NotFound();
            student.IsDeleted = true;
            await _db.SaveChangesAsync();
            return NoContent();
        }

    }
}
