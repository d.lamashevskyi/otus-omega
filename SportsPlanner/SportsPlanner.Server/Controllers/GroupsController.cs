﻿using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.AspNetCore.Authorization;
using Microsoft.AspNetCore.Mvc;
using Microsoft.EntityFrameworkCore;
using SportsPlanner.DataAccess;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.DTO;
using SportsPlanner.Server.Utils;

namespace SportsPlanner.Server.Controllers
{
    [ApiController]
    [Route("api/v1/[controller]")]
    //[Authorize(AuthenticationSchemes = JwtBearerDefaults.AuthenticationScheme/*,Roles ="Admin"*/)]
    public class GroupsController: SportsPlannerControllerBase
    {

        public GroupsController(SportsPlannerDbContext db): base(db) 
        {

        }

        [HttpGet]
        public async Task<IEnumerable<GroupShortModel>> GetGroupList()
        {
            var groups = await _db.Groups.Where(group => !group.IsDeleted).Include(group => group.Section).Include(group => group.Teacher).ToListAsync();
            return groups.Select(group => group.ToDTO<GroupShortModel>());
        }
        
        

        [HttpGet("{id:guid}")]
        public async Task<ActionResult<GroupModel>> GetGroupById(string id)
        {
            var group = await _db.Groups.Where(group => group.Id == id).Include(group => group.Section).Include(group => group.Teacher).Include(group => group.BaseSchedule).FirstOrDefaultAsync();
            if (group == null) return NotFound();

            return Ok(group.ToDTO<GroupModel>());
        }

        [HttpPut("{id:guid}")]
        public async Task<ActionResult> ModifyGroup(string id, GroupUpdateRequestModel groupData)
        {
            Group? group = await _db.Groups.Where(gr => gr.Id == id).Include(gr => gr.BaseSchedule).FirstOrDefaultAsync();
            if (group == null) return NotFound();
            group.AssignDTO(groupData);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpPost]
        public async Task<ActionResult<string>> AddGroup(GroupUpdateRequestModel groupData)
        {
            Group group = groupData.ToEntity<Group>();
            group.Id = Guid.NewGuid().ToString();
            _db.Groups.Add(group);
            await _db.SaveChangesAsync();
            return Ok(group.Id);
        }

        [HttpDelete("{id:guid}")]
        public async Task<ActionResult> DeleteGroup(string id)
        {
            var group = await _db.Groups.Where(group => group.Id == id && !group.IsDeleted).Include(group => group.Students).FirstOrDefaultAsync();
            if (group == null) return NotFound();
            group.IsDeleted = true;
            foreach(var student in group.Students)
            {
                student.IsDeleted = true;
            }
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpGet("{id:guid}/schedule")]
        public async Task<ActionResult<IEnumerable<GroupScheduleEntryModel>>> GetGroupActualSchedule(string id, [FromQuery] DateTime from, [FromQuery] DateTime to)
        {
            var group = await _db.Groups.Where(group => group.Id == id && !group.IsDeleted).Include(group => group.BaseSchedule).FirstOrDefaultAsync();
            if (group == null) return NotFound();
            var result = new List<GroupScheduleEntryModel>();

            foreach (var baseScheduleEntry in group.BaseSchedule)
            {
                DateOnly firstDate = DateOnly.FromDateTime(from) > baseScheduleEntry.BeginDate ? DateOnly.FromDateTime(from) : baseScheduleEntry.BeginDate;
                var firstDateDayOfWeek = (int)firstDate.DayOfWeek;
                if (baseScheduleEntry.DayOfWeek >= firstDateDayOfWeek)
                {
                    firstDate = firstDate.AddDays(baseScheduleEntry.DayOfWeek - firstDateDayOfWeek);
                }
                else
                {
                    firstDate = firstDate.AddDays(7 - firstDateDayOfWeek + baseScheduleEntry.DayOfWeek);
                }
                for(DateOnly curDate = firstDate; curDate <= DateOnly.FromDateTime(to) && curDate <= baseScheduleEntry.EndDate; curDate = curDate.AddDays(7))
                {
                    result.Add(new GroupScheduleEntryModel
                    {
                        LessonTime = curDate.ToDateTime(baseScheduleEntry.LessonTime),
                        Duration = baseScheduleEntry.Duration
                    });
                }
                result.Sort((e1, e2) => (int) (e1.LessonTime - e2.LessonTime).TotalMinutes);
            }
            return Ok(result);
        }

        [HttpGet("{id:guid}/students")]
        public async Task<ActionResult<IEnumerable<GroupStudentModel>>> GetGroupStudentListAsync(string id)
        {
            var group = await _db.Groups.Where(group => group.Id == id && !group.IsDeleted).Include(group => group.Students).ThenInclude(groupStudent => groupStudent.Student).FirstOrDefaultAsync();
            if (group == null) return NotFound();
            return Ok(group.Students.Where(s => !s.IsDeleted).Select(student => student.ToDTO<GroupStudentModel>()));

        }

        [HttpPost("{groupId:guid}/students")]
        public async Task<ActionResult> AddStudentToGroupAsync(string groupId, GroupStudentAdditionRequest studentData) {
            var group = await _db.Groups.Where(group => group.Id == groupId && !group.IsDeleted).Include(group => group.Students).ThenInclude(groupStudent => groupStudent.Student).FirstOrDefaultAsync();
            if (group == null) return NotFound();
            if(group.Students.FirstOrDefault(s => s.Student.Id == studentData.StudentId && !s.IsDeleted) != null)
            {
                return BadRequest(new ErrorModel("Attempt to add the student to the same group twice."));
            }
            GroupStudent newStudent = studentData.ToEntity<GroupStudent>();
            group.Students.Add(newStudent);
            await _db.SaveChangesAsync();
            return NoContent();
        }

        [HttpDelete("{groupId:guid}/students/{studentId:guid}")]
        public async Task<ActionResult> RemoveStudentFromGroupAsync(string groupId, string studentId)
        {
            var group = await _db.Groups.Where(group => group.Id == groupId && !group.IsDeleted).Include(group => group.Students).ThenInclude(groupStudent => groupStudent.Student).FirstOrDefaultAsync();
            if (group == null) return NotFound(new ErrorModel("No group with such an id"));
            var student = group.Students.FirstOrDefault(s => s.Student.Id == studentId && !s.IsDeleted);
            if (student == null)
            {
                return NotFound(new ErrorModel("No student with such an id"));
            }
            student.IsDeleted = true;
            await _db.SaveChangesAsync();
            return NoContent();

        }
    }
}
