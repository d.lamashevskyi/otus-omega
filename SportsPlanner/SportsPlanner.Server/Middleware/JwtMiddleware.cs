﻿using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Builder;
using Microsoft.AspNetCore.Components.Authorization;
using Microsoft.AspNetCore.Http;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using SportsPlanner.Models.Authentication;
using SportsPlanner.Services.JWT;
using System.IdentityModel.Tokens.Jwt;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Server.Middleware
{
    // You may need to install the Microsoft.AspNetCore.Http.Abstractions package into your project
    public class JwtMiddleware
    {
        private readonly RequestDelegate _next;
        private readonly IOptionsMonitor<JwtSchemeOptions> options;
        private string _secret;

        public JwtMiddleware(RequestDelegate next, IOptionsMonitor<JwtSchemeOptions> options)
        {
            _next = next;
            _secret = options.CurrentValue.Secret;
        }

        public Task Invoke(HttpContext httpContext, JWTService jWTService)
        {

            return _next(httpContext);
        }
        private async Task attachUserToContextAsync(HttpContext httpContext, string token)
        {
            try
            {
                var tokenHandler = new JwtSecurityTokenHandler();
                var key = Encoding.ASCII.GetBytes(_secret);
                tokenHandler.ValidateToken(token, new TokenValidationParameters
                {
                    ValidateIssuerSigningKey = true,
                    IssuerSigningKey = new SymmetricSecurityKey(key),
                    ValidateIssuer = false,
                    ValidateAudience = false,
                    // set clockskew to zero so tokens expire exactly at token expiration time (instead of 5 minutes later)
                    ClockSkew = TimeSpan.Zero,
                    NameClaimType = ClaimTypes.NameIdentifier
                }, out SecurityToken validatedToken);

                var test = tokenHandler.ReadJwtToken(token);

                var jwtToken = (JwtSecurityToken)validatedToken;
                var userId = jwtToken.Claims.First(x => x.Type == "nameid").Value;

                var claims = jwtToken.Claims;

                var identity = new ClaimsIdentity(claims);

                var principal = new ClaimsPrincipal(identity);

                var ticket = new AuthenticationTicket(principal,"Bearer");

              
                // attach user to context on successful jwt validation
               // var user = await userService.GetByIdAsync(userId);

              //  httpContext.Items["User"] = user;
                var authState = new AuthenticationState(principal);

            }
            catch
            {
                // do nothing if jwt validation fails
                // user is not attached to context so request won't have access to secure routes
            }
        }
    }

    // Extension method used to add the middleware to the HTTP request pipeline.
    public static class JwtMiddlewareExtensions
    {
        public static IApplicationBuilder UseJwtMiddleware(this IApplicationBuilder builder)
        {
            return builder.UseMiddleware<JwtMiddleware>();
        }
    }
}
