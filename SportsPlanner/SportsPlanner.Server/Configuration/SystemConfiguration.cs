﻿namespace SportsPlanner.Server.Configuration
{
    public class SystemConfiguration : ISystemConfiguration
    {
        public string CRMClientURL { get; set; }
        public string GetCRMClientURL(string CRMCode)
        {
            if (string.IsNullOrEmpty(CRMCode) || string.IsNullOrEmpty(CRMClientURL))
                return "";
            return string.Format(CRMClientURL, CRMCode);
        }
        public string CRMDealURL { get; set; }
        public string GetCRMDealURL(string CRMCode)
        {
            if (string.IsNullOrEmpty(CRMCode) || string.IsNullOrEmpty(CRMDealURL))
                return "";
            return string.Format(CRMDealURL, CRMCode);

        }
    }
}
