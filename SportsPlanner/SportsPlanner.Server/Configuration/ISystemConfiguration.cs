﻿namespace SportsPlanner.Server.Configuration
{
    public interface ISystemConfiguration
    {
        public string CRMClientURL { get; set; }
        public string GetCRMClientURL(string CRMCode);
        public string CRMDealURL { get; set; }
        public string GetCRMDealURL(string CRMCode);
    }
}
