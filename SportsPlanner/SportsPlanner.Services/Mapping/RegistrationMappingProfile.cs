﻿using AutoMapper;
using Microsoft.AspNetCore.Identity;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.Identity;
using SportsPlanner.Models.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Services.Mapping
{
    public class RegistrationMappingProfile : Profile
    {
        public RegistrationMappingProfile()
        {
            CreateMap<RegistrationRequest, BaseUser>()
                .ForMember(x => x.Email, opt => opt.MapFrom(src => src.Email))
                .ForMember(x => x.UserName, opt => opt.MapFrom(src => src.LastName))
                .ForMember(x => x.Id, opt => opt.Ignore())
                .ForMember(x => x.NormalizedUserName, opt => opt.Ignore())
                .ForMember(x => x.NormalizedEmail, opt => opt.Ignore())
                .ForMember(x => x.EmailConfirmed, opt => opt.Ignore())
                .ForMember(x => x.PasswordHash, opt => opt.Ignore())
                .ForMember(x => x.SecurityStamp, opt => opt.Ignore())
                .ForMember(x => x.ConcurrencyStamp, opt => opt.Ignore())
                .ForMember(x => x.PhoneNumber, opt => opt.Ignore())
                .ForMember(x => x.PhoneNumberConfirmed, opt => opt.Ignore())
                .ForMember(x => x.TwoFactorEnabled, opt => opt.Ignore())
                .ForMember(x => x.LockoutEnd, opt => opt.Ignore())
                .ForMember(x => x.LockoutEnabled, opt => opt.Ignore())
                .ForMember(x => x.AccessFailedCount, opt => opt.Ignore())
                .ForMember(x => x.SurName, opt => opt.Ignore())
                //.ForMember(x => x.BeginDate, opt => opt.Ignore())
                //.ForMember(x => x.EndDate, opt => opt.Ignore())
                .ForMember(x => x.IsDeleted, opt => opt.Ignore())
                .ReverseMap();
        }
    }
}
