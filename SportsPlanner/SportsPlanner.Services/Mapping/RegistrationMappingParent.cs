﻿using AutoMapper;
using SportsPlanner.DataAccess.Model;
using SportsPlanner.Models.Registration;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Services.Mapping;
public class RegistrationMappingParent : Profile
{
    public RegistrationMappingParent()
    {
        CreateMap<RegistrationRequest, Parent>()
            .ForMember(x => x.Email, opt => opt.MapFrom(src => src.Email))
            .ForMember(x => x.LastName, opt => opt.MapFrom(src => src.LastName))
            .ForMember(x => x.FirstName, opt => opt.MapFrom(src => src.FirstName))
            .ForMember(x => x.SurName, opt => opt.MapFrom(src => src.SecondName))
            .ForAllMembers(x => x.Ignore());
    }
}

