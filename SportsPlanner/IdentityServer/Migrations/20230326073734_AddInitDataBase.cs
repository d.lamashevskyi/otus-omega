﻿using Microsoft.EntityFrameworkCore.Migrations;

#nullable disable

#pragma warning disable CA1814 // Prefer jagged arrays over multidimensional

namespace IdentityServer.Migrations
{
    /// <inheritdoc />
    public partial class AddInitDataBase : Migration
    {
        /// <inheritdoc />
        protected override void Up(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "0056c958-3b4e-4e8f-a563-829956c35c55");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "fd175a71-aaca-4fb7-8d23-6e38207f9e48");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "8045fd04-665c-4ba0-9a74-764b64871131", "1be16791-0f9c-4fba-95ac-d5db14f41bb1" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "8045fd04-665c-4ba0-9a74-764b64871131");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "1be16791-0f9c-4fba-95ac-d5db14f41bb1");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "3a6673ca-25e2-4bb7-b12a-7d7866f78208", "b0573ebe-cc91-4871-a132-d2f46c9a0294", "Parent", "PARENT" },
                    { "78949091-ee08-49fb-a6fe-bb2e05f118d3", "fadb26a0-3510-44ab-a9a8-aa737f1ce0aa", "Teacher", "TEACHER" },
                    { "cedab136-eb48-4f22-bb07-8ee27adb630d", "667b0aca-249b-4326-974f-4b0ec0507f01", "Admin", "ADMIN" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "FirstName", "IsDeleted", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "SurName", "TwoFactorEnabled", "UserName" },
                values: new object[] { "4aa3755e-803f-4f9e-bf0e-8af5e2d04e59", 0, "4c063bd0-5713-4625-8952-ca59dc3a8bdd", "Admin", "admin@mail.ru", false, "Admin", false, "Admin", false, null, "ADMIN@MAIL.RU", "ADMIN@MAIL.RU", "AQAAAAEAACcQAAAAEPuH2QeGunxiqusThg7GW9YD3xIwKHsFnuwG7cv8UXrGsOcJ0K9dHLHKlfh3b41fzg==", null, false, null, "Admin", false, "admin@mail.ru" });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "cedab136-eb48-4f22-bb07-8ee27adb630d", "4aa3755e-803f-4f9e-bf0e-8af5e2d04e59" });
        }

        /// <inheritdoc />
        protected override void Down(MigrationBuilder migrationBuilder)
        {
            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "3a6673ca-25e2-4bb7-b12a-7d7866f78208");

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "78949091-ee08-49fb-a6fe-bb2e05f118d3");

            migrationBuilder.DeleteData(
                table: "AspNetUserRoles",
                keyColumns: new[] { "RoleId", "UserId" },
                keyValues: new object[] { "cedab136-eb48-4f22-bb07-8ee27adb630d", "4aa3755e-803f-4f9e-bf0e-8af5e2d04e59" });

            migrationBuilder.DeleteData(
                table: "AspNetRoles",
                keyColumn: "Id",
                keyValue: "cedab136-eb48-4f22-bb07-8ee27adb630d");

            migrationBuilder.DeleteData(
                table: "AspNetUsers",
                keyColumn: "Id",
                keyValue: "4aa3755e-803f-4f9e-bf0e-8af5e2d04e59");

            migrationBuilder.InsertData(
                table: "AspNetRoles",
                columns: new[] { "Id", "ConcurrencyStamp", "Name", "NormalizedName" },
                values: new object[,]
                {
                    { "0056c958-3b4e-4e8f-a563-829956c35c55", "07ea1058-c50a-4ad6-86e2-a4fd7d83a8e0", "Teacher", "TEACHER" },
                    { "8045fd04-665c-4ba0-9a74-764b64871131", "cef1f7a6-c1a1-44d6-bb62-70e568dd2220", "Admin", "ADMIN" },
                    { "fd175a71-aaca-4fb7-8d23-6e38207f9e48", "b13b7395-ce7a-4a2b-85b1-986e123f1ce3", "Parent", "PARENT" }
                });

            migrationBuilder.InsertData(
                table: "AspNetUsers",
                columns: new[] { "Id", "AccessFailedCount", "ConcurrencyStamp", "Discriminator", "Email", "EmailConfirmed", "FirstName", "IsDeleted", "LastName", "LockoutEnabled", "LockoutEnd", "NormalizedEmail", "NormalizedUserName", "PasswordHash", "PhoneNumber", "PhoneNumberConfirmed", "SecurityStamp", "SurName", "TwoFactorEnabled", "UserName" },
                values: new object[] { "1be16791-0f9c-4fba-95ac-d5db14f41bb1", 0, "b259e503-436e-4d3b-8a31-d161c3a30eb9", "Admin", "admin@mail.ru", true, "Admin", false, "Admin", false, null, "admin@mail.ru", "ADMIN", "63hhcgNAvKe+UI7qBhEgp4iIWe+r3SSD3Q3SIQrxkXI=", null, false, "dd53a85a-db3d-4e71-843f-086e9e095c44", "Admin", false, null });

            migrationBuilder.InsertData(
                table: "AspNetUserRoles",
                columns: new[] { "RoleId", "UserId" },
                values: new object[] { "8045fd04-665c-4ba0-9a74-764b64871131", "1be16791-0f9c-4fba-95ac-d5db14f41bb1" });
        }
    }
}
