﻿namespace IdentityServer.Models.StaticData
{

    public class StaticData
    {
        public const string AdminRole = "Admin";
        public const string TeacherRole = "Teacher";
        public const string ParentRole = "Parent";
        public const string StudentRole = "Student";
    }

}
