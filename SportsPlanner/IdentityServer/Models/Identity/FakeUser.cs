﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Models.Identity
{
    public class FakeUser
    {
        public static AuthenticateRequest User => new AuthenticateRequest { Login = "test@mail.ru", Password = "12345678" };
        public static List<string> Roles => new List<string> { "admin" };
        public static string UserId = "d8c6b722-8fc6-44f6-84ef-0a4e544a95eb";
    }
}
