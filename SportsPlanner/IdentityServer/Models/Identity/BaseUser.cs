﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace IdentityServer.Models.Identity
{
    public class BaseUser : IdentityUser
    {
        [MaxLength(100)]
        public string LastName { get; set; }
        [MaxLength(100)]
        public string FirstName { get; set; }
        [MaxLength(100)]
        public string SurName { get; set; }
        [NotMapped]
        public string FIO => $"{LastName} {FirstName} {SurName}";
        /// <summary>
        /// Признак удаления
        /// </summary>
        [Display(Name = "Признак удаления")]
        public bool IsDeleted { get; set; } = false;
    }
}
