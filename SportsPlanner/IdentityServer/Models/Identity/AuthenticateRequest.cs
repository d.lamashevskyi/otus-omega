﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.Models.Identity
{
    public class AuthenticateRequest
    {
        [Required]
        [DefaultValue("admin@mail.ru")]
        public string Login { get; set; }
        [Required]
        [DefaultValue("8PvgS6E8M4@")]
        public string Password { get; set; }
    }
}
