using Microsoft.AspNetCore.Authentication;
using Microsoft.AspNetCore.Authentication.JwtBearer;
using Microsoft.Extensions.DependencyInjection;

namespace IdentityServer.Models.Authentication
{
    public static class AuthenticationExtensions
    {
        public static AuthenticationBuilder AddCustomJwtToken(this AuthenticationBuilder builder)
        {
            return builder.AddScheme<JwtSchemeOptions, JwtSchemeHandler>(
                JwtBearerDefaults.AuthenticationScheme,
                _ => { });
        }
    }
}