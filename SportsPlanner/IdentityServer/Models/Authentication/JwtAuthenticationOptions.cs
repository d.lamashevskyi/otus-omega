using Microsoft.AspNetCore.Authentication;

namespace IdentityServer.Models.Authentication
{
    public class JwtSchemeOptions : AuthenticationSchemeOptions
    {
        public string Secret { get; set; }
    }
}