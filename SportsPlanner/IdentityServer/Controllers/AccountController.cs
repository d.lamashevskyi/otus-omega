﻿using IdentityServer.Abstractions;
using IdentityServer.DataAccess.Models;
using IdentityServer.Entities;
using IdentityServer.Models.Identity;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Mvc;
using Microsoft.Win32;
using System.Net;

namespace IdentityServer.Controllers
{
    [Route("api/v1/[controller]")]
    [ApiController]
    public class AccountController : ControllerBase
    {
        private readonly IJWTService _jwtService;
        private readonly UserManager<IdentityUser> _userManager;

        public AccountController(IJWTService jwtService, UserManager<IdentityUser> userManager)
        {
            _jwtService=jwtService;
            _userManager=userManager;
        }
        [HttpPost("signup")]
        public async Task<IActionResult> SignUpAsync(RegistrationRequest registration)
        {
            IdentityUser user;
            IdentityResult result = default!;
            if (registration.IsTeacher)
            {
                user = new Teacher { UserName = registration.Email, Email = registration.Email, LastName = registration.LastName, FirstName = registration.FirstName };
                result = await _userManager.CreateAsync(user, registration.Password);
            }
            else
            {
                user = new Parent { UserName = registration.Email, Email = registration.Email, LastName = registration.LastName, FirstName = registration.FirstName };
                result = await _userManager.CreateAsync((user as Parent)!, registration.Password);
            }


            if (result.Succeeded)
                return Ok(user);
            else
                return BadRequest(result.Errors);
        }
        [HttpPost("signin")]
        public async Task<IActionResult> SignInAsync(AuthenticateRequest model)
        {
            var response = await _jwtService.AuthenticateAsync(model);
            if (response == null)
                return Unauthorized();

            return Ok(response);
        }
    }
}
