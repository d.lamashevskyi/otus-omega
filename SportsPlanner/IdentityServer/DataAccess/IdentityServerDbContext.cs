﻿using IdentityServer.Entities;
using IdentityServer.Models.Identity;
using IdentityServer.Models.StaticData;
using Microsoft.AspNetCore.Cryptography.KeyDerivation;
using Microsoft.AspNetCore.Identity;
using Microsoft.AspNetCore.Identity.EntityFrameworkCore;
using Microsoft.EntityFrameworkCore;
using System.Security.Cryptography;

namespace IdentityServer.DataAccess
{
    public class IdentityServerDbContext : IdentityDbContext
    {
        public DbSet<Parent> Parents { get; set; }
        public DbSet<Teacher> Teachers { get; set; }
        public DbSet<Admin> Admins { get; set; }
        public IdentityServerDbContext(DbContextOptions<IdentityServerDbContext> options) : base(options)
        {
          //  Database.EnsureDeleted();
           // Database.EnsureCreated();
        }

        protected override void OnModelCreating(ModelBuilder builder)
        {
            InitDataBase(builder);
            base.OnModelCreating(builder);
        }
        protected override void OnConfiguring(DbContextOptionsBuilder optionsBuilder)
        {
            base.OnConfiguring(optionsBuilder);
        }

        private void InitDataBase(ModelBuilder builder)
        {
            var adminRoleid = Guid.NewGuid().ToString();
            var adminUserid = Guid.NewGuid().ToString();
            builder.Entity<IdentityRole>().HasData(new IdentityRole[]
            {
              new  IdentityRole{ Id = adminRoleid, Name = StaticData.AdminRole, NormalizedName = StaticData.AdminRole.ToUpper()},
              new  IdentityRole{Id = Guid.NewGuid().ToString(),  Name = StaticData.TeacherRole, NormalizedName = StaticData.TeacherRole.ToUpper()},
              new  IdentityRole{Id = Guid.NewGuid().ToString(),  Name = StaticData.ParentRole, NormalizedName = StaticData.ParentRole.ToUpper()}
            });

            builder.Entity<Admin>().HasData(new Admin[]
            {
                 new Admin{
                     Id = adminUserid,
                     LastName ="Admin",
                     FirstName="Admin",
                     SurName="Admin",
                     EmailConfirmed = false,
                     UserName = "admin@mail.ru",
                     Email = "admin@mail.ru",
                     NormalizedEmail = "ADMIN@MAIL.RU",
                     NormalizedUserName = "ADMIN@MAIL.RU",
                     PasswordHash = new PasswordHasher<IdentityUser>().HashPassword(null,"8PvgS6E8M4@"),
                     SecurityStamp = null
                 }
            });

            builder.Entity<IdentityUserRole<string>>().HasData(new IdentityUserRole<string>[]
            {
                 new IdentityUserRole<string>{ RoleId = adminRoleid, UserId = adminUserid}
            });

        }
    }
}
