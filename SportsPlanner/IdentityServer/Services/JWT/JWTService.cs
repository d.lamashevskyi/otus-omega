﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using IdentityServer.Abstractions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityServer.Models.Identity;
using IdentityServer.Models.Authentication;
using IdentityModel;
using IdentityServer.Models.StaticData;

namespace IdentityServer.Services
{
    public class JWTService : IJWTService
    {
        private string _secret;

        private readonly UserManager<IdentityUser>? _userManager;
        private readonly IOptionsMonitor<JwtSchemeOptions> options;
        public JWTService(UserManager<IdentityUser> userManager
                          , IOptionsMonitor<JwtSchemeOptions> options)
        {
            _userManager=userManager;
            _secret = options.CurrentValue.Secret;
        }

        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            if (model is null) return null;

            var user = await _userManager.FindByEmailAsync(model.Login);

            if (user == null) return null;
            var result = await _userManager.CheckPasswordAsync(user, model.Password);

            if (!result) return null;
            //if (!await _userManager.IsEmailConfirmedAsync(user))
            //    return await Task.FromResult(new AuthenticateResponse() { ErrMsg = "Почта не подтверждена" });


            var token = await GenerateJwtTokenAsync(user);

            // authentication successful so generate jwt token
            return await Task.FromResult(new AuthenticateResponse() { Login = user.Email, Token = token });
        }
        private async Task<string> GenerateJwtTokenAsync(IdentityUser user)
        {
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_secret);

            var roles = await _userManager.GetRolesAsync(user);
            var roleClaims = new List<Claim>();
            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim(JwtClaimTypes.Role, roles[i]));
            }
            roleClaims.Add(new Claim(JwtClaimTypes.Role, StaticData.ParentRole));

            var claims = new[] { new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()) }
                .Union(roleClaims);


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
