﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Models.DTO
{
    public class ErrorModel
    {
        public ErrorModel() { ErrorMessage = string.Empty; }
        public ErrorModel(string message) { ErrorMessage = message; }
        public string ErrorMessage { get; set; }
    }
}
