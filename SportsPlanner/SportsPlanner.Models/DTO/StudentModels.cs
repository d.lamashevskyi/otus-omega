﻿using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Xml.Linq;
using System.Text.RegularExpressions;

namespace SportsPlanner.Models.DTO;


public class StudentUpdateRequestModel
{
    public string LastName { get; set; }

    public string FirstName { get; set; }

    public string? SurName { get; set; }
    public DateOnly? Birthdate { get; set; }
    public string? ParentId { get; set; }
}

public class StudentShortModel : StudentUpdateRequestModel
{
    public string Id { get; set; }


}

public class StudentModel: StudentShortModel { 
    
    public ICollection<StudentGroupModel> Groups { get; set; }
    public bool IsDeleted { get; set; }
    
}

public class StudentGroupModel
{
    public  GroupShortModel Group{ get; set; }
    public int PaidLessonsRest { get; set; }

}

