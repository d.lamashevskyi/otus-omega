﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.Models.DTO
{
    public class SectionUpdateRequestModel
    {
        [Required]
        public string Name { get; set; }
        public string Description { get; set; }

    }

    public class SectionShortModel: SectionUpdateRequestModel
    {
        public string Id { get; set; }

    }

    public class SectionModel: SectionShortModel
    {
        public bool IsDeleted { get; set; }

    }
}
