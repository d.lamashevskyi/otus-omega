﻿namespace SportsPlanner.Models.DTO;

public class GroupStudentAdditionRequest
{
    public string StudentId { get; set; }
    public int PaidLessonsRest { get; set; }

}

public class GroupStudentModel : GroupStudentAdditionRequest
{
    public StudentShortModel Student {get; set; }
}