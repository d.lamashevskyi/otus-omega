﻿namespace SportsPlanner.Server.Controllers
{
    public class ParentModel
    {
        public string Id { get; set; }
        public string LastName { get; set; }
        public string FirstName { get; set; }
        public string SurName { get; set; }
        public string? Email { get; set; }
        public string? Telegram { get; set; }
        public string? Phone { get; set; }
    }
}