﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Models.DTO
{

    public class GroupUpdateRequestModel
    {
        public string Name { get; set; }

        public string SectionId { get; set; }

        public string TeacherId { get; set; }

        public ICollection<GroupBaseScheduleEntryModel> BaseSchedule { get; set; }

    }

    public class GroupShortModel 
    {
        public string Id { get; set; }
        public string Name { get; set; }

        public string SectionId { get; set; }

        public string TeacherId { get; set; }

        public SectionShortModel Section { get; set; }

        public TeacherShortModel Teacher { get; set; }
    }

    public class GroupModel: GroupShortModel
    {
        public ICollection<GroupBaseScheduleEntryModel> BaseSchedule { get; set; }  = new List<GroupBaseScheduleEntryModel>();
        public bool IsDeleted { get; set; }
    }

    public class GroupBaseScheduleEntryModel
    {
        public int DayOfWeek { get; set; }
        public TimeOnly LessonTime { get; set; }
        public int Duration { get; set; }
        public DateOnly BeginDate { get; set; }
        public DateOnly? EndDate { get; set; }
    }

    public class GroupScheduleEntryModel
    {
        public DateTime LessonTime { get; set; }
        public int Duration { get; set; }
    }

}
