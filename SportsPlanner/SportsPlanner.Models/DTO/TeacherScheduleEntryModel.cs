﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Models.DTO
{
    public class TeacherScheduleEntryModel
    {
        DateTime StartTime { get; set; }
        int Duration { get; set; }        
        GroupShortModel Group { get; set; }

    }
}
