﻿using Microsoft.AspNetCore.Identity;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Xml.Linq;

namespace SportsPlanner.Models.Identity
{
    public class BaseUser : IdentityUser
    {
        [MaxLength(100)]
        [Display(Name = "Фамилия")]
        public string LastName { get; set; }
        [MaxLength(100)]
        [Display(Name = "Имя")]
        public string FirstName { get; set; }
        [MaxLength(100)]
        [Display(Name = "Отчество")]
        public string SurName { get; set; }
        [Display(Name = "ФИО")]
        [NotMapped]
        public string Name => $"{LastName} {FirstName} {SurName}";
        /// <summary>
        /// Признак удаления
        /// </summary>
        [Display(Name = "Признак удаления")]
        public bool IsDeleted { get; set; } = false;
    }
}
