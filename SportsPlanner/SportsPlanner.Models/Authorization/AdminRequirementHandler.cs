﻿using Microsoft.AspNetCore.Authorization.Infrastructure;
using Microsoft.AspNetCore.Authorization;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using System.IO;
using Microsoft.AspNetCore.Identity;
using SportsPlanner.Models.Identity;
using System.Security.Claims;
using Microsoft.IdentityModel.JsonWebTokens;

namespace SportsPlanner.Models.Authorization
{
    public class AdminRequirementHandler : AuthorizationHandler<AdminRequirement>
    {
        protected override Task HandleRequirementAsync(AuthorizationHandlerContext context, AdminRequirement requirement)
        {
            bool isAuthorized = context.User.FindFirstValue("role") ==  "admin";
            if (isAuthorized)
                context.Succeed(requirement);
            else
                context.Fail();


            return Task.CompletedTask;
        }
    }
}
