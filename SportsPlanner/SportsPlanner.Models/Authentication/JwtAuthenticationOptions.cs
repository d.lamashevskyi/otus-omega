using Microsoft.AspNetCore.Authentication;

namespace SportsPlanner.Models.Authentication
{
    public class JwtSchemeOptions : AuthenticationSchemeOptions
    {

        public string Secret { get; set; }
    }
}