﻿using Microsoft.AspNetCore.Identity;
using Microsoft.Extensions.Options;
using Microsoft.IdentityModel.Tokens;
using IdentityServer.Core.Abstractions;
using System;
using System.Collections.Generic;
using System.IdentityModel.Tokens.Jwt;
using System.Linq;
using System.Security.Claims;
using System.Text;
using System.Threading.Tasks;
using IdentityServer.DataAccess.Models;

namespace IdentityServer.Services
{
    public class JWTService : IJWTService
    {
        private string _secret;

        private readonly UserManager<IdentityUser>? _userManager;
        private readonly IOptionsMonitor<JwtSchemeOptions> options;
        public JWTService(UserManager<IdentityUser> userManager
                        , IOptionsMonitor<JwtSchemeOptions> options)
        {
            _userManager=userManager;
            _secret = options.CurrentValue.Secret;
        }

        public async Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model)
        {
            model = FakeUser.User;
            if (model is null) return null;

            var user = new IdentityUser { Email = model.Login }; //await _userManager.FindByEmailAsync(model.Login);

            // if (user == null) return null;
            // var result = await _signInManager.CheckPasswordSignInAsync(user, model.Password, false);

            //if (!result.Succeeded)
            //    return null;
            //if (!await _userManager.IsEmailConfirmedAsync(user))
            //    return await Task.FromResult(new AuthenticateResponse() { ErrMsg = "Почта не подтверждена" });


            var token = GenerateJwtToken(user);

            // authentication successful so generate jwt token
            return await Task.FromResult(new AuthenticateResponse() { Login = user.Email, Token = token });
        }
        private string GenerateJwtToken(IdentityUser user)
        {
            user.Id = FakeUser.UserId;
            var tokenHandler = new JwtSecurityTokenHandler();

            var key = Encoding.ASCII.GetBytes(_secret);

            var roles = FakeUser.Roles;// await _userManager.GetRolesAsync(user);
            var roleClaims = new List<Claim>();
            for (int i = 0; i < roles.Count; i++)
            {
                roleClaims.Add(new Claim(ClaimTypes.Role, roles[i]));
            }

            var claims = new[] { new Claim(ClaimTypes.NameIdentifier, user.Id.ToString()) }
                .Union(roleClaims);


            var tokenDescriptor = new SecurityTokenDescriptor
            {
                Subject = new ClaimsIdentity(claims),
                Expires = DateTime.UtcNow.AddMinutes(10),
                SigningCredentials = new SigningCredentials(new SymmetricSecurityKey(key), SecurityAlgorithms.HmacSha256),
            };

            var token = tokenHandler.CreateToken(tokenDescriptor);
            return tokenHandler.WriteToken(token);
        }
    }
}
