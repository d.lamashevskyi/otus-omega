﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Core.Abstractions.Repositories
{
    public interface IRepository<T>
    {
        Task<IEnumerable<T>> GetAllAsync();
        Task<T> GetByIdAsync(string id);
        Task<IEnumerable<T>> GetRangeByIdsAsync(List<string> ids);
        //Task<T> GetFirstWhere(Expression<Func<T, bool>> predicate);
        //Task<IEnumerable<T>> GetWhere(Expression<Func<T, bool>> predicate);
        Task AddAsync(T entity);
        Task UpdateAsync(T entity);
        Task DeleteAsync(T entity);
        Task isDeleteAsync(T entity);
    }
}
