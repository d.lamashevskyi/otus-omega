﻿using SportsPlanner.Models.Identity;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace SportsPlanner.Core.Abstractions
{
    public interface IJWTService
    {
        Task<AuthenticateResponse> AuthenticateAsync(AuthenticateRequest model);
        string ValidateJwtToken(string token);
    }
}
