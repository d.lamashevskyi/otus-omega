using Microsoft.AspNetCore.Authentication;

namespace IdentityServer.DataAccess.Models  
{
    public class JwtSchemeOptions : AuthenticationSchemeOptions
    {
        public string Secret { get; set; }
    }
}