﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace IdentityServer.DataAccess.Models
{
    public class AuthenticateRequest
    {
        [Required]
        [DefaultValue("test@mail.ru")]
        public string Login { get; set; }
        [Required]
        [DefaultValue("12345678")]
        public string Password { get; set; }
    }
}
